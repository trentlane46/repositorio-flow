

import cv2
#import numpy
from matplotlib import pyplot as plt

img = cv2.imread('si2.jpg',0)
img2 = img.copy()

(row, col) = img.shape

f_max = img2.max();
f_min = img2.min();

img3 = img2.copy();
for i in range (row):
    for j in range(col):
        img3[i][j] = ((img [i][j] - f_min)/ (f_max - f_min)) * 256;


 
fig, axs = plt.subplots(nrows = 2, ncols = 2, figsize =(8,8))
axs[0][0].imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
axs[0][0].axis("off")

axs[0][1].imshow(img2, cmap="Greys")
axs[0][1].axis("off")

axs[1][1].imshow(img3, cmap="gray")
axs[1][1].axis("off")

plt.show();
